ARG IMG_TAG=8.13.0
FROM atlassian/jira-core:$IMG_TAG
RUN chown -R ${RUN_USER}:root        ${JIRA_INSTALL_DIR} \
    && chmod -R 775                  ${JIRA_INSTALL_DIR}/temp \
    && chmod -R 775                  ${JIRA_INSTALL_DIR}/work \
    && chmod -R 775                  ${JIRA_INSTALL_DIR}/logs  